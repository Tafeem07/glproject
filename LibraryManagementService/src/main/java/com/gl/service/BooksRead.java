package com.gl.service;

import java.util.List;

import com.gl.model.Books;

public interface BooksRead {

	List<Books> getAllBooks();

    Books getBooksById(int id);

    List<Books> getBooksByName(String bookName);

    Books getBooksByLastAdded();

    List<Books> getBooksByAuthor(String author);

    List<Books> getBooksByAscendingOrder();

    Books addBooks(Books book);

    void deleteById(int id);

    void deleteAllBooks();


}
